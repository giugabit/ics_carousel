ics_carousel è un modulo joomla pensato per il sito https://wwww.icamericgovespucci.edu.it. Lo scopo è quello di mettere in evidenza alcuni contenuti sulla homepage del sito.

Per la costruizione di un modulo joomla custom è stata presa come riferimento questa guida: https://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module

Per ricreare lo zip da pubblicare sul sito basta lanciare il il bat *zipIt.bat*

Il bat è pensato per piattaforma windows e parte dal presupposto che sia installato 7zip nella locazione: *c:\Program Files\7-Zip\7z.exe*

Il modulo va abilitato sulla sola pagina homepage del sito.