function openUrl(url, targetBlank){
	var anchor = document.createElement('a');
	anchor.href = url;
	if(targetBlank){
		anchor.target="_blank";
	}
	anchor.click();
}