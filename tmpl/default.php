<?php 
// No direct access
defined('_JEXEC') or die; ?>

<link rel="stylesheet" type="text/css" href="modules/mod_ics_carousel/ics_carousel.css">
<link rel="stylesheet" type="text/css" href="modules/mod_ics_carousel/ics_highlighted_box.css">
<script src="modules/mod_ics_carousel/ics_highlighted_box.js"></script>
<script src="modules/mod_ics_carousel/ics_carousel.js"></script>

<style>
	.ics_carousel .header-line {
		<?php echo "transition: width $articleTiming"."s;" ?>
	}
</style>

<div class="ics_carousel">
	<?php if($backgroundImg != NULL) { echo "<img class='ics_car_background' src=$backgroundImg></img>"; } ?>
	<div class="ics_car_content" onClick="carousel.openArticle()">
		<div class="header-line"></div>
		<div class="faText ics_car_text"></div>
	</div>
	<div class="f_dots">
		<?php
			foreach ($fArticles as $fArticle) {
				$id = $fArticle['id'];
				echo "<span id='f_dot_$id' class='f_dot f_dot_inactive' onClick='carousel.jumpTo($id)'></span>";
			}
		?>
	</div>
</div>

<?php
	echo "<script>";
	echo "var featuredArticles = new FuturedArticles();\n";
	foreach ($fArticles as $fArticle) {
		$id = $fArticle['id'];
		$title = addslashes($fArticle['title']);
		$url = $fArticle['url'];
		echo "var fa = new FeaturedArticle($id, '$url', '$title');\n";
		echo "featuredArticles.add(fa);\n";
	}
	echo "var carousel = new Carousel(featuredArticles, $targetBlank, $articleTimingStop);\n";
	echo "carousel.start();\n";
	echo "</script>";
?>

<?php if($hasBoxes){
	echo "<div id=\"ics_highlighted_box\">";
	echo "	<div class=\"icsf_boxes\">";
	$count = 0;
	foreach ($boxes as $box) {
		if($count>=3){
			break;
		}
		$type = $box['type'];
		$text = $box['text'];
		$img = $box['img'];
		$url = $box['url'];
		$targetBlank = $box['targetBlank'] == 1 ? 'true' : 'false';
		echo "		<div class=\"box-content\" onClick=\"openUrl('".$url."', ".$targetBlank.");\">";
		if($type=="Immagine"){
			echo "<img src=\"".$img."\" title=\"".$text."\"/>";
		}else{
			echo "<span>".$text."</span>";
		}
		echo "		</div>";
		$count++;
	}
	echo "	</div>";
	echo "</div>";
}