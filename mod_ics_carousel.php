<?php
/**
 * 
 * @package    ics.vespucci
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 */

// No direct access
defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$backgroundImg = $params->get('backgroundImg');
$numArticles = $params->get('numArticles');
$articleTiming = $params->get('articleTiming');
$articleTimingStop = $params->get('articleTimingStop');
$targetBlank = $params->get('targetBlank');
$fArticles = modICSCarouselHelper::getFeaturedArticles($numArticles);


$blockActive1 = $params->get('blockActive1');
$blockActive2 = $params->get('blockActive2');
$blockActive3 = $params->get('blockActive3');
$blockActive4 = $params->get('blockActive4');
$blockActive5 = $params->get('blockActive5');
$blockActive6 = $params->get('blockActive6');

$hasBoxes = $blockActive1==1 || $blockActive2==1 || $blockActive3==1 || $blockActive4==1 || $blockActive5==1 || $blockActive6==1;
$boxes = array();

if($blockActive1==1){
	array_push($boxes, array("type" => $params->get('boxType1'), "text" => $params->get('boxText1'), "img" => $params->get('boxImage1'), "url" => $params->get('boxUrl1'), "targetBlank" =>  $params->get('boxTargetBlank1')));
}

if($blockActive2==1){
	array_push($boxes, array("type" => $params->get('boxType2'), "text" => $params->get('boxText2'), "url" => $params->get('boxUrl2'), "img" => $params->get('boxImage2'), "targetBlank" =>  $params->get('boxTargetBlank2')));
}

if($blockActive3==1){
	array_push($boxes, array("type" => $params->get('boxType3'), "text" => $params->get('boxText3'), "url" => $params->get('boxUrl3'), "img" => $params->get('boxImage3'), "targetBlank" =>  $params->get('boxTargetBlank3')));
}

if($blockActive4==1){
	array_push($boxes, array("type" => $params->get('boxType4'), "text" => $params->get('boxText4'), "url" => $params->get('boxUrl4'), "img" => $params->get('boxImage4'), "targetBlank" =>  $params->get('boxTargetBlank4')));
}

if($blockActive5==1){
	array_push($boxes, array("type" => $params->get('boxType5'), "text" => $params->get('boxText5'), "url" => $params->get('boxUrl5'), "img" => $params->get('boxImage5'), "targetBlank" =>  $params->get('boxTargetBlank5')));
}

if($blockActive6==1){
	array_push($boxes, array("type" => $params->get('boxType6'), "text" => $params->get('boxText6'), "url" => $params->get('boxUrl6'), "img" => $params->get('boxImage6'), "targetBlank" =>  $params->get('boxTargetBlank6')));
}

require JModuleHelper::getLayoutPath('mod_ics_carousel');