var FeaturedArticle = function(id, url, title) {
	this.id = id;
	this.url = url;
	this.title = title;
}

var FuturedArticles = function() {
	
	this.fas = [];
	
	this.currentIndex = -1;
	
	this.add = function(featuredArticle) {
		this.fas.push(featuredArticle);
	}
	
	this.next = function() {
		this.currentIndex++;
		if(this.currentIndex === this.fas.length) {
			this.currentIndex = 0;
		}
		return this.fas[this.currentIndex];
	}
	
	this.jumpTo = function(id){
		var index = 0;
		for(var i = 0; i < this.fas.length; i++){
			var fea = this.fas[i];
			if(fea.id === id){
				this.currentIndex = i;
				return fea;
			}
		}
	}
	
	this.initForRestart = function(){
		this.currentIndex--;
	}
	
}

var Carousel = function(futuredArticles, targetBlank, articleTimingStop) {
	this.futuredArticles = futuredArticles;
	this.targetBlank = targetBlank;
	this.articleTimingStop = articleTimingStop;
	var self = this;
	
	this.start = function() {
		this.next();
	}
	
	this.next = function() {		
		var t = document.getElementsByClassName('header-line')[0];
		t.classList.add("reset-tra");
		t.classList.remove("header-line-tra");
		
		setTimeout(this.innerNext, 500);
	}
	
	this.innerNext = function (){
		var currentFA = self.futuredArticles.next();
		self.bind(currentFA);
	}
	
	this.bind = function(featuredArticle){
		var faTextEl = document.getElementsByClassName("faText")[0];
		var target = this.targetBlank ? "target='_blank'" : "";
		faTextEl.innerHTML="<a id='articleLink' href='"+featuredArticle.url+"' "+target+">"+featuredArticle.title+"</a>";
		
		var t = document.getElementsByClassName('header-line')[0];
		t.classList.remove("reset-tra");
		t.classList.add("header-line-tra");
		
		this.activeteDot(featuredArticle);
		
		const testLineEl = document.querySelector('.header-line');
		testLineEl.ontransitionend = () => {
			this.next();
		};
	}
	
	this.activeteDot = function(featuredArticle){
		Array.from(document.getElementsByClassName("f_dot")).forEach(
			function(dot, index, array) {
				dot.classList.remove("f_dot_active");
				dot.classList.add("f_dot_inactive");
			}
		);

		var toActivateDot = document.getElementById('f_dot_'+featuredArticle.id);
		toActivateDot.classList.remove("f_dot_inactive");
		toActivateDot.classList.add("f_dot_active");
	}
	
	this.openArticle = function() {
		var articleLink = document.getElementById('articleLink');
		if(articleLink){
			articleLink.click();	
		}
	}
	
	this.jumpTo = function(id){
		var t = document.getElementsByClassName('header-line')[0];
		t.classList.add("reset-tra");
		t.classList.remove("header-line-tra");
		
		var featuredArticle = this.futuredArticles.jumpTo(id);
		var faTextEl = document.getElementsByClassName("faText")[0];
		var target = this.targetBlank ? "target='_blank'" : "";
		faTextEl.innerHTML="<a id='articleLink' href='"+featuredArticle.url+"' "+target+">"+featuredArticle.title+"</a>";
		
		this.activeteDot(featuredArticle);
		
		setTimeout(this.restart, this.articleTimingStop+"000");
	}
	
	this.restart = function(){
		self.futuredArticles.initForRestart();
		self.next();
	}
	
}