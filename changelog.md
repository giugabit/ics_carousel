# 1.0.0

*  Articoli in evidenza
    *  Numero di articoli in evidenza configurabile da 1 a 5
    *  Barra di scorrimento che prennuncia passaggio ad articolo successivo
    *  Durata articolo configurabile da 0 a 20 secondi
    *  Pallino cliccabile per boloccare temporaneaente lo scorrimento
    *  Durata stop a seguito di click del pallino configurabile da 0 a 20 secondi
    *  Backgroud configurabile
    *  Configurazione apertura articolo *target=_blank* *true/false* 
	
# 2.0.0

*  Boxes
    * Boxes da evidenziare su homepage
    * Getsione testo
	* Gestione immagine
	* Geestione apertura su pagina esterna
	* Max tre box