<?php
/**
 * 
 * @package    ics.vespucci
 * @subpackage Modules
 * @license    GNU/GPL, see LICENSE.php
 */
class ModICSCarouselHelper
{
	
	public static function getFeaturedArticles($count){
		$db = JFactory::getDBO();

		$sql = "SELECT c.id, c.title FROM #__content c join #__content_frontpage cf on c.id = cf.content_id WHERE c.featured = 1 order by cf.ordering limit ".$count;
		$db->setQuery($sql);
		$result = $db->loadAssocList();
		
		foreach ($result as &$item) {
			$id = $item['id'];
			$item['url'] = modICSCarouselHelper::getArticleUrl($id);
		}
		
		return $result;
	}
	
	public static function getArticleUrl($articleId){
		return $url = "index.php?option=com_content&view=article&id=$articleId";
	}
	
}